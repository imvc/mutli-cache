mutli-cache
======================
-------------

#简介#

##Java多级缓存框架##

L1： 进程内缓存(ehcache)
L2： 集中式缓存，支持多种集中式缓存服务器，如 Redis


缓存任意切换
可使用单一缓存，ehcache或redis
支持redis集群，代码无需任何改动

支持 JGroups 和 Redis Subscribe 两种方式进行缓存同步。在某些云平台上可能无法使用 JGroups 组播方式，可以采用 Redis 发布订阅的方式



使用方式
classpath下建立cache.properties文件（参考resource/cache.properties）


		String cacheName = "person";
		CacheClient client = CacheClient.getInstance();
		client.put(cacheName, "10001", new Person("董超", 26));
		Person p = client.get(cacheName, "10001");
		System.out.println(p);

		List<String> keys = client.keys(cacheName);
		System.out.println(Arrays.toString(keys.toArray()));

		client.update(cacheName, "10002", new Person("董李文", 1));
		p = client.get(cacheName, "10002");
		System.out.println(p);

		keys = client.keys(cacheName);
		System.out.println(Arrays.toString(keys.toArray()));

		client.remove(cacheName, "10001");

		keys = client.keys(cacheName);
		System.out.println(Arrays.toString(keys.toArray()));
		
		client.clear(cacheName);
		keys = client.keys(cacheName);
		System.out.println(Arrays.toString(keys.toArray()));



package com.puff.cache;

public interface CallBack<T> {

	public T call(String key);

}

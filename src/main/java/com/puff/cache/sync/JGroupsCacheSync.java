package com.puff.cache.sync;

import java.net.URL;
import java.util.List;

import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JGroupsCacheSync extends ReceiverAdapter implements CacheSync {

	private static final Logger log = LoggerFactory.getLogger(JGroupsCacheSync.class);
	private static final String channelName = "MutliCache-Channel";
	private static final String CONFIGXML = "/network.xml";
	private JChannel channel;
	private CacheSyncHandler cacheSync = new CacheSyncHandler();

	public JGroupsCacheSync() {
		super();
	}

	@Override
	public CacheSync init() {
		System.setProperty("java.net.preferIPv4Stack", "true");
		try {
			long start = System.currentTimeMillis();
			URL xml = JGroupsCacheSync.class.getResource(CONFIGXML);
			if (xml == null) {
				log.warn("Can not find jgroups config file 'network.xml' in classpath, use default config in jar...");
				xml = JGroupsCacheSync.class.getClassLoader().getResource("com/puff/cache/sync" + CONFIGXML);
			}
			channel = new JChannel(xml);
			channel.setReceiver(this);
			channel.connect(channelName);
			log.info("Connected to JGroups channel: " + channelName + " success. time " + (System.currentTimeMillis() - start) + " ms.");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return this;
	}

	@Override
	public void sendCommand(Command command) {
		try {
			channel.send(new Message().setBuffer(command.toBuffers()));
		} catch (Exception e) {
			log.error("send command fail ...", e);
		}
	}

	/**
	 * 接收消息
	 */
	@Override
	public void receive(Message msg) {
		byte[] buffers = msg.getBuffer();
		// 忽略消息
		if (buffers == null || buffers.length < 1) {
			log.warn("Message is empty.");
			return;
		}
		if (!msg.getSrc().equals(channel.getAddress())) {
			try {
				Command cmd = Command.parse(buffers);
				if (cmd != null) {
					cacheSync.handle(cmd);
				}
			} catch (Exception e) {
				log.error("Handel received msg error...", e);
			}
		}
	}

	/**
	 * 组中成员变化时
	 */
	@Override
	public void viewAccepted(View view) {
		List<Address> addrs = view.getMembers();
		int size = addrs.size();
		StringBuffer sb = new StringBuffer("Group Members Changed, Size[" + size + "]. List : ");
		for (int i = 0; i < size; i++) {
			if (i > 0) {
				sb.append(',');
			}
			sb.append(addrs.get(i).toString());
		}
		log.info(sb.toString());
	}

}

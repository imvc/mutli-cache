package com.puff.cache;

public class StrKit {

	public static boolean empty(final String str) {
		return (str == null) || (str.length() == 0);
	}

	public static boolean notEmpty(final String str) {
		return !empty(str);
	}

	public static boolean blank(final String str) {
		int strLen;
		if ((str == null) || ((strLen = str.length()) == 0))
			return true;
		for (int i = 0; i < strLen; i++) {
			if (!Character.isWhitespace(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public static boolean notBlank(final String str) {
		return !blank(str);
	}

	public static boolean allEmpty(String... strings) {
		if (strings == null) {
			return true;
		}
		for (String str : strings) {
			if (notEmpty(str)) {
				return false;
			}
		}
		return true;
	}

	public static boolean hasEmpty(String... strings) {
		if (strings == null) {
			return true;
		}
		for (String str : strings) {
			if (empty(str)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * checkValue为 null 或者为 "" 时返回 defaultValue
	 */
	public static String empty(String checkValue, String defaultValue) {
		return empty(checkValue) ? defaultValue : checkValue;
	}

	/**
	 * 字符串不为 null 而且不为 "" 并且等于other
	 */
	public static boolean notEmptyAndEqOther(String str, String other) {
		if (empty(str)) {
			return false;
		}
		return str.equals(other);
	}

	/**
	 * 字符串不为 null 而且不为 "" 并且不等于other
	 */
	public static boolean notEmptyAndNotEqOther(String str, String... other) {
		if (empty(str)) {
			return false;
		}
		for (int i = 0; i < other.length; i++) {
			if (str.equals(other[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 字符串不等于other
	 */
	public static boolean notEqOther(String str, String... other) {
		for (int i = 0; i < other.length; i++) {
			if (other[i].equals(str)) {
				return false;
			}
		}
		return true;
	}

	public static boolean notEmpty(String... strings) {
		if (strings == null) {
			return false;
		}
		for (String str : strings) {
			if (str == null || "".equals(str.trim())) {
				return false;
			}
		}
		return true;
	}

	public static boolean equals(String value, String equals) {
		if (allEmpty(value, equals)) {
			return true;
		}
		return value.equals(equals);
	}

	public static boolean notEquals(String value, String equals) {
		return !equals(value, equals);
	}

}

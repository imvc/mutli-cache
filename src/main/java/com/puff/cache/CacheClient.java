package com.puff.cache;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.puff.cache.ehcache.EHCacheManager;
import com.puff.cache.multilevel.MultiLevelCacheManager;
import com.puff.cache.redis.RedisCacheManager;
import com.puff.cache.sync.CacheSync;

/**
 * cache 客户端
 * @author dongchao
 *
 */
public class CacheClient {

	private static final Logger log = LoggerFactory.getLogger(CacheClient.class);
	private CacheManager cacheManager;
	private CacheManager firstCacheManager;// 一级缓存
	private CacheManager secondCacheManager;// 二级缓存
	private MultiLevelCacheManager multiCacheManager;// 多级缓存
	private final List<String> cacheType = Arrays.asList("ehcache", "redis");// 支持缓存类型
	private final List<String> secondCacheType = Arrays.asList("redis");// 二级缓存支持类型
	private final List<String> cacheSyncType = Arrays.asList("jgroups", "redis");

	private CacheClient() {
		Properties prop = new Properties();
		try {
			
			Properties propertiesPath = new Properties();
			propertiesPath.load(CacheClient.class.getResourceAsStream("/constants.properties"));
			prop.load(new FileInputStream(propertiesPath.getProperty("cache.properties")));
			
		} catch (IOException e) {
			String err = " Can not be found cache config file cache.properties, please put cache.properties in classpath ";
			log.error(err);
			throw new RuntimeException(err);
		}
		String cache = prop.getProperty("cache.manager");
		String l1_cache = prop.getProperty("cache.level1.manager");
		String l2_cache = prop.getProperty("cache.level2.manager");

		if (StrKit.allEmpty(cache, l1_cache, l2_cache)) {
			String err = " Cache Manager can not be null...please check your config', Currently only supported " + cacheType.toString();
			log.error(err);
			throw new IllegalArgumentException(err);
		}
		boolean singleCache = false;
		if (StrKit.notEmpty(cache)) {
			if (!cacheType.contains(cache)) {
				String err = "Unknown cache Manager '" + cache + "', Currently only supported " + cacheType.toString();
				log.error(err);
				throw new IllegalArgumentException(err);
			}
			singleCache = true;
		}
		boolean cache_1 = false;
		if (StrKit.notEmpty(l1_cache)) {
			if (!cacheType.contains(l1_cache)) {
				String err = "Unknown cache Manager '" + l1_cache + "', Currently only supported " + cacheType.toString();
				log.error(err);
				throw new IllegalArgumentException(err);
			}
			cache_1 = true;
		}
		boolean cache_2 = false;
		if (StrKit.notEmpty(l2_cache)) {
			if (!cacheType.contains(l2_cache)) {
				String err = "Unknown cache Manager '" + l2_cache + "', Currently only supported " + cacheType.toString();
				log.error(err);
				throw new IllegalArgumentException(err);
			}
			if (cache_1) {
				if (!secondCacheType.contains(l2_cache)) {
					String err = "Unknown cache Manager '" + l2_cache + "',Level2 cache currently only supported 'redis'";
					log.error(err);
					throw new IllegalArgumentException(err);
				}
			}
			cache_2 = true;
		}
		if (cache_1 && cache_2) {
			if (l1_cache.equals(l2_cache)) {
				String err = "Level1 cache can not as same as the level2 cache ";
				log.error(err);
				throw new IllegalArgumentException(err);
			}
			firstCacheManager = getCacheManager(l1_cache);
			log.info("Using Level1 CacheManager : " + firstCacheManager.getClass().getName());
			secondCacheManager = getCacheManager(l2_cache);
			log.info("Using Level2 CacheManager : " + secondCacheManager.getClass().getName());
			multiCacheManager = new MultiLevelCacheManager(firstCacheManager, secondCacheManager);
			multiCacheManager.start(prop);
			String cacheSync = prop.getProperty("cache.sync");
			if (StrKit.notBlank(cacheSync)) {
				cacheSync = cacheSync.toLowerCase();
				if (cacheSyncType.contains(cacheSync)) {
					CacheSync sync = null;
					try {
						if ("redis".equals(cacheSync)) {
							sync = (CacheSync) Class.forName("com.puff.cache.sync.RedisCacheSync").newInstance();
						}
						if ("jgroups".equals(cacheSync)) {
							sync = (CacheSync) Class.forName("com.puff.cache.sync.JGroupsCacheSync").newInstance();
						}
					} catch (Exception e) {
					}
					multiCacheManager.setCacheSync(sync.init());
				}
			}
			cacheManager = multiCacheManager;
		} else {// single cache
			if (singleCache) {
				cacheManager = getCacheManager(cache);
			} else if (cache_1) {
				cacheManager = getCacheManager(l1_cache);
			} else if (cache_2) {
				cacheManager = getCacheManager(l2_cache);
			}
			cacheManager.start(prop);
			firstCacheManager = cacheManager;
			secondCacheManager = cacheManager;
			log.info("Using CacheManager : " + cacheManager.getClass().getName());
		}
	}

	private CacheManager getCacheManager(String value) {
		if ("ehcache".equalsIgnoreCase(value))
			return new EHCacheManager();
		if ("redis".equalsIgnoreCase(value)) {
			return new RedisCacheManager();
		}
		return new EHCacheManager();
	}

	private static class Inner {
		private static final CacheClient INSTANCE = new CacheClient();
	}

	public final static CacheClient getCacheClient() {
		return Inner.INSTANCE;
	}

	public final static CacheClient getInstance() {
		return Inner.INSTANCE;
	}

	/**
	 * 获取缓存
	 * @param key
	 * @return
	 */
	public <T extends Serializable> T get(String cacheName, String key) {
		Cache cache = cacheManager.buildCache(cacheName);
		return cache.get(key);
	}

	/**
	 * 获取缓存,同时删除
	 * @param key
	 * @return
	 */
	public <T extends Serializable> T getAndRemove(String cacheName, String key) {
		Cache cache = cacheManager.buildCache(cacheName);
		return cache.getAndRemove(key);
	}

	/**
	 * 获取缓存(从一级缓存获取)
	 * @param key
	 * @return
	 */
	public <T extends Serializable> T getFirst(String cacheName, String key) {
		Cache cache = firstCacheManager.buildCache(cacheName);
		return cache.get(key);
	}

	/**
	 * 获取缓存(从一级缓存获取),同时删除
	 * @param key
	 * @return
	 */
	public <T extends Serializable> T getAndRemoveFirst(String cacheName, String key) {
		Cache cache = firstCacheManager.buildCache(cacheName);
		return cache.getAndRemove(key);
	}

	/**
	 * 获取缓存(从二级缓存获取)
	 * @param key
	 * @return
	 */
	public <T extends Serializable> T getSecond(String cacheName, String key) {
		Cache cache = secondCacheManager.buildCache(cacheName);
		return cache.get(key);
	}

	/**
	 * 获取缓存(从二级缓存获取),同时删除
	 * @param key
	 * @return
	 */
	public <T extends Serializable> T getAndRemoveSecond(String cacheName, String key) {
		Cache cache = secondCacheManager.buildCache(cacheName);
		return cache.getAndRemove(key);
	}

	/**
	 * 获取缓存
	 * @param key
	 * @return
	 */
	public <T extends Serializable> List<T> get(String cacheName, List<String> keys) {
		Cache cache = cacheManager.buildCache(cacheName);
		return cache.get(keys);
	}

	/**
	 * 获取缓存(从一级缓存获取)
	 * @param key
	 * @return
	 */
	public <T extends Serializable> List<T> getFirst(String cacheName, List<String> keys) {
		Cache cache = firstCacheManager.buildCache(cacheName);
		return cache.get(keys);
	}

	/**
	 * 获取缓存(从二级缓存获取)
	 * @param key
	 * @return
	 */
	public <T extends Serializable> List<T> getSecond(String cacheName, List<String> keys) {
		Cache cache = secondCacheManager.buildCache(cacheName);
		return cache.get(keys);
	}

	/**
	 * 获取缓存，取不到时调用CallBack接口获取数据
	 * @param key
	 * @param callBack
	 * @return
	 */
	private <T extends Serializable> T get(Cache cache, String key, CallBack<T> callBack) {
		return cache.get(key, callBack);
	}

	/**
	 * 获取缓存，取不到时调用CallBack接口获取数据
	 * @param key
	 * @param callBack
	 * @return
	 */
	public <T extends Serializable> T get(String cacheName, String key, CallBack<T> callBack) {
		Cache cache = cacheManager.buildCache(cacheName);
		return get(cache, key, callBack);
	}

	/**
	 * 获取缓存(从一级缓存获取)，取不到时调用CallBack接口获取数据
	 * @param key
	 * @param callBack
	 * @return
	 */
	public <T extends Serializable> T getFirst(String cacheName, String key, CallBack<T> callBack) {
		Cache cache = firstCacheManager.buildCache(cacheName);
		return get(cache, key, callBack);
	}

	/**
	 * 获取缓存(从二级缓存获取)，取不到时调用CallBack接口获取数据
	 * @param key
	 * @param callBack
	 * @return
	 */
	public <T extends Serializable> T getSecond(String cacheName, String key, CallBack<T> callBack) {
		Cache cache = secondCacheManager.buildCache(cacheName);
		return get(cache, key, callBack);
	}

	/**
	 * 储存缓存，key存在直接覆盖
	 * @param <T>
	 * @param key
	 * @param value
	 */
	private <T extends Serializable> void put(Cache cache, String key, T value) {
		cache.put(key, value);
	}

	/**
	 * 储存缓存，key存在直接覆盖
	 * @param <T>
	 * @param key
	 * @param value
	 */
	public <T extends Serializable> void put(String cacheName, String key, T value) {
		Cache cache = cacheManager.buildCache(cacheName);
		put(cache, key, value);
	}

	/**
	 * 储存缓存(直接储存在一级缓存)，key存在直接覆盖
	 * @param <T>
	 * @param key
	 * @param value
	 */
	public <T extends Serializable> void putFirst(String cacheName, String key, T value) {
		Cache cache = firstCacheManager.buildCache(cacheName);
		put(cache, key, value);
	}

	/**
	 * 储存缓存(直接储存在二级缓存)，key存在直接覆盖
	 * @param <T>
	 * @param key
	 * @param value
	 */
	public <T extends Serializable> void putSecond(String cacheName, String key, T value) {
		Cache cache = secondCacheManager.buildCache(cacheName);
		put(cache, key, value);
	}

	/**
	 * 储存缓存，key存在直接覆盖，同时设置超时时间
	 * @param <T>
	 * @param key
	 * @param value
	 * @param expire
	 */
	private <T extends Serializable> void put(Cache cache, String key, T value, int expire) {
		cache.put(key, value, expire);
	}

	/**
	 * 储存缓存，key存在直接覆盖，同时设置超时时间
	 * @param <T>
	 * @param key
	 * @param value
	 * @param expire
	 */
	public <T extends Serializable> void put(String cacheName, String key, T value, int expire) {
		Cache cache = cacheManager.buildCache(cacheName);
		put(cache, key, value, expire);
	}

	/**
	 * 储存缓存(直接储存在一级缓存)，key存在直接覆盖，同时设置超时时间
	 * @param <T>
	 * @param key
	 * @param value
	 * @param expire
	 */
	public <T extends Serializable> void putFirst(String cacheName, String key, T value, int expire) {
		Cache cache = firstCacheManager.buildCache(cacheName);
		put(cache, key, value, expire);
	}

	/**
	 * 储存缓存(直接储存在二级缓存)，key存在直接覆盖，同时设置超时时间
	 * @param <T>
	 * @param key
	 * @param value
	 * @param expire
	 */
	public <T extends Serializable> void putSecond(String cacheName, String key, T value, int expire) {
		Cache cache = secondCacheManager.buildCache(cacheName);
		put(cache, key, value, expire);
	}

	/**
	 * 更新缓存，保留缓存超时时间 比如a有效期10分钟 5分钟后更新a a的超时时间还是5分钟
	 * @param <T>
	 * @param key
	 * @param value
	 */
	private <T extends Serializable> void update(Cache cache, String key, T value) {
		cache.update(key, value);
	}

	/**
	 * 更新缓存，保留缓存超时时间 比如a有效期10分钟 5分钟后更新a a的超时时间还是5分钟
	 * @param <T>
	 * @param key
	 * @param value
	 */
	public <T extends Serializable> void update(String cacheName, String key, T value) {
		Cache cache = cacheManager.buildCache(cacheName);
		update(cache, key, value);
	}

	/**
	 * 更新缓存(直接操作一级缓存)，保留缓存超时时间 比如a有效期10分钟 5分钟后更新a a的超时时间还是5分钟
	 * @param <T>
	 * @param key
	 * @param value
	 */
	public <T extends Serializable> void updateFirst(String cacheName, String key, T value) {
		Cache cache = firstCacheManager.buildCache(cacheName);
		update(cache, key, value);
	}

	/**
	 * 更新缓存(直接操作二级缓存)，保留缓存超时时间 比如a有效期10分钟 5分钟后更新a a的超时时间还是5分钟
	 * @param <T>
	 * @param key
	 * @param value
	 */
	public <T extends Serializable> void updateSecond(String cacheName, String key, T value) {
		Cache cache = secondCacheManager.buildCache(cacheName);
		update(cache, key, value);
	}

	/**
	 * 获取缓存有效时间
	 * @param key
	 * @return
	 */
	private int ttl(Cache cache, String key) {
		return cache.ttl(key);
	}

	/**
	 * 获取缓存有效时间
	 * @param key
	 * @return
	 */
	public int ttl(String cacheName, String key) {
		Cache cache = cacheManager.buildCache(cacheName);
		return ttl(cache, key);
	}

	/**
	 * 获取缓存有效时间(直接操作一级缓存)
	 * @param key
	 * @return
	 */
	public int ttlFirst(String cacheName, String key) {
		Cache cache = firstCacheManager.buildCache(cacheName);
		return ttl(cache, key);
	}

	/**
	 * 获取缓存有效时间(直接操作二级缓存)
	 * @param key
	 * @return
	 */
	public int ttlSecond(String cacheName, String key) {
		Cache cache = secondCacheManager.buildCache(cacheName);
		return ttl(cache, key);
	}

	/**
	 * 获取 cacheName所有key
	 * @return
	 */
	private List<String> keys(Cache cache) {
		return cache.keys();
	}

	/**
	 * 获取 cacheName所有key
	 * @return
	 */
	public List<String> keys(String cacheName) {
		Cache cache = cacheManager.buildCache(cacheName);
		return keys(cache);
	}

	/**
	 * 获取 group所有key(直接操作一级缓存)
	 * @return
	 */
	public List<String> keysFirst(String cacheName) {
		Cache cache = firstCacheManager.buildCache(cacheName);
		return keys(cache);
	}

	/**
	 * 获取 group所有key(直接操作二级缓存)
	 * @return
	 */
	public List<String> keysSecond(String cacheName) {
		Cache cache = secondCacheManager.buildCache(cacheName);
		return keys(cache);
	}

	/**
	 * 移除缓存
	 * @param key
	 */
	private void remove(Cache cache, String key) {
		cache.remove(key);
	}

	/**
	 * 移除缓存
	 * @param key
	 */
	public void remove(String cacheName, String key) {
		remove(cacheManager.buildCache(cacheName), key);
	}

	/**
	 * 移除缓存(直接操作一级缓存)
	 * @param key
	 */
	public void removeFirst(String cacheName, String key) {
		remove(firstCacheManager.buildCache(cacheName), key);
	}

	/**
	 * 移除缓存(直接操作二级缓存)
	 * @param key
	 */
	public void removeSecond(String cacheName, String key) {
		remove(secondCacheManager.buildCache(cacheName), key);
	}

	/**
	 * 批量移除
	 * @param keys
	 */
	private void remove(Cache cache, List<String> keys) {
		cache.remove(keys);
	}

	/**
	 * 批量移除
	 * @param keys
	 */
	public void remove(String cacheName, List<String> keys) {
		remove(cacheManager.buildCache(cacheName), keys);
	}

	/**
	 * 批量移除(直接操作一级缓存)
	 * @param keys
	 */
	public void removeFirst(String cacheName, List<String> keys) {
		remove(firstCacheManager.buildCache(cacheName), keys);
	}

	/**
	 * 批量移除(直接操作二级缓存)
	 * @param keys
	 */
	public void removeSecond(String cacheName, List<String> keys) {
		remove(secondCacheManager.buildCache(cacheName), keys);
	}

	/**
	 * 清除cacheName内缓存
	 */
	private void clear(Cache cache) {
		cache.clear();
	}

	/**
	 * 清除cacheName内缓存
	 */
	public void clear(String cacheName) {
		clear(cacheManager.buildCache(cacheName));
	}

	/**
	 * 清除cacheName内缓存(直接操作一级缓存)
	 */
	public void clearFirst(String cacheName) {
		clear(firstCacheManager.buildCache(cacheName));
	}

	/**
	 * 清除cacheName内缓存(直接操作二级缓存)
	 */
	public void clearSecond(String cacheName) {
		clear(secondCacheManager.buildCache(cacheName));
	}

}

package com.puff.cache;

import java.util.Arrays;
import java.util.List;

import com.puff.cache.ehcache.EHCacheManager;

public class EHCacheTest {

	public static void main(String[] args) {
		EHCacheManager cacheManager = new EHCacheManager();
		cacheManager.start(null);
		Cache cache = cacheManager.buildCache("Person");
		cache.put("name", "dongchao");
		cache.put("asdasdas", "dongchao");
		cache.put("d", "dongchao");
		cache.put("nasdasdasdasdasdame", "dongchao");
		cache.put("age", 20, 20);

		List<String> keys = cache.keys();
		System.out.println(Arrays.toString(keys.toArray()));

//		cache.clear();

		while (true) {
			int ttl = cache.ttl("age");
			System.out.println(ttl);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (ttl <= 0)
				break;
		}

	}

}

package com.puff.cache;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.jgroups.stack.GossipType;

import com.puff.cache.redis.Redis;

public class CacheClientTest {

	public static void main(String[] args) {
		getKey("person");
	}
	public static void getKey(String key) {
		String cacheName = key;
		final CacheClient client = CacheClient.getInstance();
		
		while(true){
			if (Redis.getInstance().setnx(key+"", "aa") == 1) {

				Redis.getInstance().expire(key+"", 20);
				
				
				
				
				Redis.getInstance().remove(key+"");
				
			} else {
				System.out.println("i am not come in======================");
				
				
			}
		}

	}
}

class Person implements Serializable {
	private static final long serialVersionUID = 551035198657864035L;
	private String name;
	private int age;

	public Person(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}